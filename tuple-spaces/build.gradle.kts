task<JavaExec>("makeDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.DiceBuilder"
}

task<JavaExec>("cleanDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.DiceCleaner"
    standardInput = System.`in`
}

task<JavaExec>("rollDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.DiceRoller"
    standardInput = System.`in`
}

task<JavaExec>("stopDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.DiceStopper"
    standardInput = System.`in`
}
