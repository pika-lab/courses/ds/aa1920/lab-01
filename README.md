# Linda over TuCSoN:

### General remarks

* Always remember to run:

   ```bash
   ./gradlew --stop
   ```

   after each demo is completed, in order to ensure all TuCSoN nodes are actually shut down.

* To start a local TuCSoN node:

   ```bash
   ./gradlew tucson -Pport=<node port>
   ```
   before each demo in order to switch on a working TuCSoN node on your machine.

* To start a TuCSoN CLI:

   ```bash
   ./gradlew cli -Pport=<node port>
   ```

* To start a TuCSoN inspector:

   ```bash
   ./gradlew inspector
   ```

## Lab 1 / Exercise 1 – `out`, `rd`, `in`

* Switch on local TuCSoN node

* Start CLI

* Write

   ```prolog
   out(hello)
   rd(hello)
   in(hello)
   ```

* Switch off CLI by writing

   ```prolog
   exit
   ```

* Switch off TuCSoN


## Lab 1 / Exercise 2 – Generative Communication

* Switch on local TuCSoN node

* Launch Inspector

* Proceed with default, "Inspect" to open Inspector, select "Tuple Space"

* Launch CLI

* Write

   ```prolog
   out(hello)
   rd(hello)
   in(hello)
   ```

* Observe tuple space via Inspector

* Switch off CLI and Inspector

* Switch off TuCSoN


## Lab 1 / Exercise 3 – Associative Access

* Switch on a local TuCSoN node

* Start one CLI and one Inspector

* Write

   ```prolog
   out(msg(gio, hello))
   out(msg(ste, world))
   rd(msg(gio, _))
   rd(msg(ste, _))
   ```

* Switch off CLI and Inspector

* Switch off TuCSoN


## Lab 1 / Exercise 4 – Suspensive Semantics

* Switch on local TuCSoN node

* Start two CLI and one Inspector

* Write

   ```prolog
   out(msg(gio, hello))
   rd(msg(ste, _))
   rd(msg(gio, _))
   out(msg(ste, world))
   ```

* Switch off CLIs and Inspector

* Switch off TuCSoN


## Lab 1 / Exercise 5 – Non-determinism

* Switch on local TuCSoN node

* Start one CLI and one Inspector

* Write

   ```prolog
   out(prime(37))
   out(prime(47))
   out(prime(23))
   out(prime(17))
   rd(prime(_))
   rd(prime(_))
   rd(prime(_))
   in(prime(_))
   rd(prime(_))
   rd(prime(_))
   rd(prime(_))
   ```

* Write

   ```prolog
   in_all(prime(_),_)
   ```

  to clear the tuple space

* Switch off CLI and Inspector

* Switch off TuCSoN


## Lab 1 / Exercise 5bis – Uniform Non-determinism

* Switch on local TuCSoN node

* Start one CLI and one Inspector

* Write

   ```prolog
   out(prime(37))
   out(prime(47))
   out(prime(23))
   out(prime(17))
   urd(prime(_))
   urd(prime(_))
   urd(prime(_))
   uin(prime(_))
   urd(prime(_))
   urd(prime(_))
   urd(prime(_))
   ```

* Write

   ```prolog
   in_all(prime(_),_)
   ```

  to clear the tuple space

* Switch off CLI and Inspector

* Switch off TuCSoN
